import {Metrics} from "./metrics";
var StatsdClient = require("statsd-client");

export class Statsd implements Metrics {
    private sdc: any;

    constructor(prefix: string, address: string, port?: number) {
        if(!port) {
            port = 8125;
        }
        let cfg = {
            prefix: prefix,
            host: address,
            port: port
        };
        this.sdc = new StatsdClient(cfg);
    }
    gauge(name:string, value:number):void {
        this.sdc.gauge(name, value);
    }

    increment(name:string):void {
        this.sdc.increment(name, 1);
    }

    incrementBy(name:string, value:number):void {
        this.sdc.increment(name, value);
    }

    decrement(name:string):void {
        this.sdc.decrement(name, 1);
    }

    decrementBy(name:string, value:number):void {
        this.sdc.decrement(name, value);
    }

    duration(name:string, duration:number|Date):void {
        // sdc.timing() accepts both, but typscript isn't happy, so cast to any.
        this.sdc.timing(name, <any>duration);
    }
}
