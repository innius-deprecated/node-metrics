import {Metrics} from "./metrics";
export class Noop implements Metrics {
    gauge(name:string, value:number):void {
        return;
    }

    increment(name:string):void {
        return;
    }

    incrementBy(name:string, value:number):void {
        return;
    }

    decrement(name:string):void {
        return;
    }

    decrementBy(name:string, value:number):void {
        return;
    }

    duration(name:string, duration:number|Date):void {
        return;
    }
}
