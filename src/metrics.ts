export interface Metrics {
    /**
     * Record an arbitrary value at a given counter
     * @param name The bucket
     * @param value The value to record
     */
    gauge(name: string, value: number): void;
    /**
     * Increment a given counter
     * @param name The bucket
     */
    increment(name: string): void;
    /**
     * Increment a given counter by a specified value
     * @param name The bucket
     * @param value The value to increment by
     */
    incrementBy(name: string, value: number): void;
    /**
     * Decrement a given counter
     * @param name The bucket
     */
    decrement(name: string): void;
    /**
     * Decrement a given counter by a specified value
     * @param name The bucket
     * @param value The value to decrement with
     */
    decrementBy(name: string, value: number): void;
    /**
     * Record a duration of something
     * @param name The bucket
     * @param duration A duration in milliseconds, or a Date object indicating the start of something
     */
    duration(name:string, duration:number|Date): void;
}
