import {Metrics} from "./metrics";
import {Noop} from "./noop";
import {Statsd} from "./statsd";

/**
 * Initialize the metrics package.
 * @param prefix The prefix for the metrics
 * @param address An address to sent the metrics to.
 * @param port An optional port.
 */
export function initialize(prefix: string, address:string, port?:number):void {
    State.initialize(prefix, address, port);
}

class State implements Metrics {
    private static _instance:State = new State();

    private impl:Metrics = new Noop();

    constructor() {
        if (State._instance) {
            throw new Error("Error: Singleton is not allowed to be newly created by calling the constructor");
        }
        State._instance = this;
    }

    /**
     * Initialize this metrics state singleton.
     * @param prefix The prefix for the metrics
     * @param address A mandatory address to send the metrics to.
     * @param port
     */
    public static initialize(prefix: string, address:string, port?:number):void {
        State._instance.impl = new Statsd(prefix, address, port);
    }

    public static getInstance():State {
        return State._instance;
    }

    gauge(name:string, value:number):void {
        this.impl.gauge(name, value);
    }

    increment(name:string):void {
        this.impl.increment(name);
    }

    incrementBy(name:string, value:number):void {
        this.impl.incrementBy(name, value);
    }

    decrement(name:string):void {
        this.impl.decrement(name);
    }

    decrementBy(name:string, value:number):void {
        this.impl.decrementBy(name, value);
    }

    duration(name:string, duration:number|Date):void {
        this.impl.duration(name, duration);
    }
}

export var metrics:Metrics = State.getInstance();
